﻿using FoodSalesWEB.Helpers;
using FoodSalesWEB.Models;
using FoodSalesWEB.Models.API;
using FoodSalesWEB.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Razor.Tokenizer;
using System.Xml.Linq;

namespace FoodSalesWEB.Controllers
{
    public class HomeController : Controller  
    {
        public ActionResult Index()
        {
            FoodModels foodModels = new FoodModels();
            try
            {
                iHttpClient iHttpClient = new iHttpClient();

                #region Get Category
                var resultCategory = iHttpClient.GetAsync(UrlHelpers.categoryList);
                if (resultCategory.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_category>>>(resultCategory.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Category.ToString(), Value = e.Category.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodModels.SelectCategoryList = li;
                    }
                }
                else
                {
                    ViewBag.MsgError = resultCategory.Message;
                    return View(foodModels);
                }
                #endregion

                #region Get City
                var resultCity = iHttpClient.GetAsync(UrlHelpers.cityList);
                if (resultCity.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_city>>>(resultCity.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.City.ToString(), Value = e.City.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodModels.SelectCityList = li;
                    }
                }
                else
                {
                    ViewBag.MsgError = resultCity.Message;
                    return View(foodModels);
                }
                #endregion

                #region Get region
                var resultRegion = iHttpClient.GetAsync(UrlHelpers.regionList);
                if (resultRegion.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_region>>>(resultRegion.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Region.ToString(), Value = e.Region.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodModels.SelectRegionList = li;
                    }
                }
                else
                {
                    ViewBag.MsgError = resultRegion.Message;
                    return View(foodModels);
                }
                #endregion

                #region Get Data In Table
                var resultData = iHttpClient.GetAsync(UrlHelpers.foodList);
                if (resultData.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<m_food>>>(resultData.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    }
                    else
                    {
                        foodModels.m_Foods = new List<m_food>();
                        foodModels.m_Foods = resultModel.data;
                    }
                    return View(foodModels);
                }
                else
                {
                    ViewBag.MsgError = resultData.Message;
                    return View(foodModels);
                }
                #endregion

            }
            catch (Exception ex)
            {
                ViewBag.MsgError = ex.Message.Replace("\r\n", "");
                return View();
            }
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Index")]
        public ActionResult Index(FoodModels foodModels)
        {
            try
            {
                iHttpClient iHttpClient = new iHttpClient();

                #region Get Category
                var resultCategory = iHttpClient.GetAsync(UrlHelpers.categoryList);
                if (resultCategory.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_category>>>(resultCategory.Message);
                    if (resultModel.error_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Category.ToString(), Value = e.Category.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodModels.SelectCategoryList = li;
                    }
                }
                else
                {
                    ViewBag.MsgError = resultCategory.Message;
                    return View(foodModels);
                }
                #endregion

                #region Get City
                var resultCity = iHttpClient.GetAsync(UrlHelpers.cityList);
                if (resultCity.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_city>>>(resultCity.Message);
                    if (resultModel.error_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.City.ToString(), Value = e.City.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodModels.SelectCityList = li;
                    }
                }
                else
                {
                    ViewBag.MsgError = resultCity.Message;
                    return View(foodModels);
                }
                #endregion

                #region Get region
                var resultRegion = iHttpClient.GetAsync(UrlHelpers.regionList);
                if (resultRegion.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_region>>>(resultRegion.Message);
                    if (resultModel.error_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Region.ToString(), Value = e.Region.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodModels.SelectRegionList = li;
                    }
                }
                else
                {
                    ViewBag.MsgError = resultRegion.Message;
                    return View(foodModels);
                }
                #endregion
            }
            catch (Exception ex)
            {
                ViewBag.MsgError = ex.Message.Replace("\r\n", "");

            }
            return View(foodModels);
        }

        public ActionResult Delete(int foodID)
        {
            try
            {
                iHttpClient iHttpClient = new iHttpClient();
                var result = iHttpClient.GetDeleteAsync(UrlHelpers.foodDelete + foodID + "/" + true);
                if (result.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<bool>>(result.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        ViewBag.UrlGoTo = Url.Action("Index", "Home");
                        return View("Index");
                    }
                    else
                    {
                        ViewBag.MsgSuccess = resultModel.message;
                        ViewBag.UrlGoTo = Url.Action("Index", "Home");
                        return View("Index");
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = result.Message;
                    return View("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.MsgError = ex.Message.Replace("\r\n", "");
                ViewBag.UrlGoTo = Url.Action("Index", "Home");
                return View("Index");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        public ActionResult Search(FoodModels FoodModels)
        {
            try
            {
                iHttpClient iHttpClient = new iHttpClient();

                #region Get Category
                var resultCategory = iHttpClient.GetAsync(UrlHelpers.categoryList);
                if (resultCategory.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_category>>>(resultCategory.Message);
                    if (resultModel.error_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(FoodModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Category.ToString(), Value = e.Category.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        FoodModels.SelectCategoryList = li;
                    }
                }
                else
                {
                    ViewBag.MsgError = resultCategory.Message;
                    return View(FoodModels);
                }
                #endregion

                #region Get City
                var resultCity = iHttpClient.GetAsync(UrlHelpers.cityList);
                if (resultCity.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_city>>>(resultCity.Message);
                    if (resultModel.error_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(FoodModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.City.ToString(), Value = e.City.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        FoodModels.SelectCityList = li;
                    }
                }
                else
                {
                    ViewBag.MsgError = resultCity.Message;
                    return View(FoodModels);
                }
                #endregion

                #region Get region
                var resultRegion = iHttpClient.GetAsync(UrlHelpers.regionList);
                if (resultRegion.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_region>>>(resultRegion.Message);
                    if (resultModel.error_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(FoodModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Region.ToString(), Value = e.Region.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        FoodModels.SelectRegionList = li;
                    }
                }
                else
                {
                    ViewBag.MsgError = resultRegion.Message;
                    return View(FoodModels);
                }
                #endregion

                if (FoodModels.OrderDateEnd == null && FoodModels.OrderDateStart == null && FoodModels.ProductNameSearch == null &&
                     FoodModels.SelectCategory == null && FoodModels.SelectCity == null && FoodModels.SelectRegion == null)
                {
                    ViewBag.MsgWarning = "กรุณาเลือกเงื่อนไขการค้นหา";
                    return View("Index", FoodModels);
                }


                FoodSearchModelsIn foodSearchModelsIn = new FoodSearchModelsIn()
                {
                    OrderDateEnd = FoodModels.OrderDateEnd,
                    OrderDateStart = FoodModels.OrderDateStart,
                    ProductNameSearch = FoodModels.ProductNameSearch,
                    SearchCategory = FoodModels.SelectCategory,
                    SearchCity = FoodModels.SelectCity,
                    SearchRegion = FoodModels.SelectRegion
                };

                

                #region Get Data In Table
                var resultData = iHttpClient.GetPostAsync(UrlHelpers.foodSearch, foodSearchModelsIn);
                if (resultData.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<m_food>>>(resultData.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View("Index", FoodModels);
                    }
                    else
                    {
                        FoodModels.m_Foods = new List<m_food>();
                        FoodModels.m_Foods = resultModel.data;
                        return View("Index", FoodModels);
                    }
                }
                else
                {
                    ViewBag.MsgError = resultData.Message;
                    return View("Index", FoodModels);
                }
                #endregion

            }
            catch (Exception ex)
            {
                ViewBag.MsgError = ex.Message.Replace("\r\n", "");

            }
            return View("Index",FoodModels);
        }
    }
}