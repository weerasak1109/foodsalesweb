﻿using FoodSalesWEB.Models.API;
using FoodSalesWEB.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Net;
using FoodSalesWEB.Helpers;
using System.Text;

namespace FoodSalesWEB.Controllers
{
    public class iHttpClient
    {
        string iUrl = UrlHelpers.urlFirstPart;


        public iHttpClientModel GetAsync(string requestUri)
        {
            try 
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(iUrl);
                    //HTTP GET
                    var responseTask = client.GetAsync(requestUri);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        iHttpClientModel iHttpClient = new iHttpClientModel()
                        {
                            Message = result.Content.ReadAsStringAsync().Result,
                            StatusCode = (int)result.StatusCode
                        };
                        return iHttpClient;
                    }
                    else
                    {
                        iHttpClientModel iHttpClient = new iHttpClientModel()
                        {
                            Message = @"Request failed.Error status code: " + (int)result.StatusCode,
                            StatusCode = (int)result.StatusCode
                        };
                        return iHttpClient;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public iHttpClientModel GetPostAsync(string requestUri, object val)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var json = JsonConvert.SerializeObject(val);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");

                    client.BaseAddress = new Uri(iUrl);
                    //HTTP Post
                    var responseTask = client.PostAsync(requestUri, data);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        iHttpClientModel iHttpClient = new iHttpClientModel()
                        {
                            Message = result.Content.ReadAsStringAsync().Result,
                            StatusCode = (int)result.StatusCode
                        };
                        return iHttpClient;
                    }
                    else
                    {
                        iHttpClientModel iHttpClient = new iHttpClientModel()
                        {
                            Message = @"Request failed.Error status code: " + (int)result.StatusCode,
                            StatusCode = (int)result.StatusCode
                        };
                        return iHttpClient;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public iHttpClientModel GetDeleteAsync(string requestUri)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(iUrl);
                    //HTTP delete
                    var responseTask = client.DeleteAsync(requestUri);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        iHttpClientModel iHttpClient = new iHttpClientModel()
                        {
                            Message = result.Content.ReadAsStringAsync().Result,
                            StatusCode = (int)result.StatusCode
                        };
                        return iHttpClient;
                    }
                    else
                    {
                        iHttpClientModel iHttpClient = new iHttpClientModel()
                        {
                            Message = @"Request failed.Error status code: " + (int)result.StatusCode,
                            StatusCode = (int)result.StatusCode
                        };
                        return iHttpClient;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}