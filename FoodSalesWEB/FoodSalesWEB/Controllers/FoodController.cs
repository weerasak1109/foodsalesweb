﻿using FoodSalesWEB.Helpers;
using FoodSalesWEB.Models;
using FoodSalesWEB.Models.API;
using FoodSalesWEB.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FoodSalesWEB.Controllers
{
    public class FoodController : Controller
    {
        public ActionResult Edit(int foodID)
        {
            FoodInfoModels foodInfoModels = new FoodInfoModels();
            try
            {
                iHttpClient iHttpClient = new iHttpClient();

                #region Get Category
                var resultCategory = iHttpClient.GetAsync(UrlHelpers.categoryList);
                if (resultCategory.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_category>>>(resultCategory.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Category.ToString(), Value = e.Category.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectCategoryList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultCategory.Message;
                    return View(foodInfoModels);
                }
                #endregion

                #region Get City
                var resultCity = iHttpClient.GetAsync(UrlHelpers.cityList);
                if (resultCity.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_city>>>(resultCity.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.City.ToString(), Value = e.City.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectCityList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultCity.Message;
                    return View(foodInfoModels);
                }
                #endregion

                #region Get region
                var resultRegion = iHttpClient.GetAsync(UrlHelpers.regionList);
                if (resultRegion.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_region>>>(resultRegion.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Region.ToString(), Value = e.Region.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectRegionList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultRegion.Message;
                    return View(foodInfoModels);
                }
                #endregion

                #region Get Data Info
                var resultData = iHttpClient.GetAsync(UrlHelpers.foodByID + foodID);
                if (resultData.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<m_food>>(resultData.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    }
                    else
                    {
                        foodInfoModels.m_Food = new mFood()
                        {
                            Quantity = resultModel.data.Quantity,
                            Category = resultModel.data.Category,
                            City = resultModel.data.City,
                            foodID = resultModel.data.foodID,
                            OrderDate = resultModel.data.OrderDate,
                            Product = resultModel.data.Product,
                            Region = resultModel.data.Region,
                            TotalPrice = resultModel.data.TotalPrice,
                            UnitPrice = resultModel.data.UnitPrice
                        };
                    }
                    return View(foodInfoModels);
                }
                else
                {
                    ViewBag.MsgError = resultData.Message;
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    return View(foodInfoModels);
                }
                #endregion
            }
            catch (Exception ex)
            {
                ViewBag.UrlGoTo = Url.Action("Index", "Home");
                ViewBag.MsgError = ex.Message.Replace("\r\n", "");
                return View(foodInfoModels);
            }

        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit(FoodInfoModels foodInfoModels)
        {
            try
            {
                iHttpClient iHttpClient = new iHttpClient();

                #region Get Category
                var resultCategory = iHttpClient.GetAsync(UrlHelpers.categoryList);
                if (resultCategory.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_category>>>(resultCategory.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Category.ToString(), Value = e.Category.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectCategoryList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultCategory.Message;
                    return View(foodInfoModels);
                }
                #endregion

                #region Get City
                var resultCity = iHttpClient.GetAsync(UrlHelpers.cityList);
                if (resultCity.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_city>>>(resultCity.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.City.ToString(), Value = e.City.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectCityList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultCity.Message;
                    return View(foodInfoModels);
                }
                #endregion

                #region Get region
                var resultRegion = iHttpClient.GetAsync(UrlHelpers.regionList);
                if (resultRegion.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_region>>>(resultRegion.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Region.ToString(), Value = e.Region.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectRegionList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultRegion.Message;
                    return View(foodInfoModels);
                }
                #endregion

                foodInfoModels.m_Food.TotalPrice = foodInfoModels.m_Food.Quantity * foodInfoModels.m_Food.UnitPrice;

                var result = iHttpClient.GetPostAsync(UrlHelpers.foodEdit, foodInfoModels.m_Food);
                if (result.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<bool>>(result.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        ViewBag.UrlGoTo = Url.Action("Index", "Home");
                        return View(foodInfoModels);
                    }
                    else
                    {
                        ViewBag.MsgSuccess = resultModel.message;
                        ViewBag.UrlGoTo = Url.Action("Index", "Home");
                        return View(foodInfoModels);
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = result.Message;
                    return View(foodInfoModels);
                }

            }
            catch (Exception ex)
            {
                ViewBag.MsgError = ex.Message.Replace("\r\n", "");
                ViewBag.UrlGoTo = Url.Action("Index", "Home");
            }
            return View(foodInfoModels);
        }

        public ActionResult Add()
        {
            FoodInfoModels foodInfoModels = new FoodInfoModels();
            try
            {
                iHttpClient iHttpClient = new iHttpClient();

                #region Get Category
                var resultCategory = iHttpClient.GetAsync(UrlHelpers.categoryList);
                if (resultCategory.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_category>>>(resultCategory.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Category.ToString(), Value = e.Category.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectCategoryList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultCategory.Message;
                    return View(foodInfoModels);
                }
                #endregion

                #region Get City
                var resultCity = iHttpClient.GetAsync(UrlHelpers.cityList);
                if (resultCity.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_city>>>(resultCity.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.City.ToString(), Value = e.City.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectCityList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultCity.Message;
                    return View(foodInfoModels);
                }
                #endregion

                #region Get region
                var resultRegion = iHttpClient.GetAsync(UrlHelpers.regionList);
                if (resultRegion.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_region>>>(resultRegion.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Region.ToString(), Value = e.Region.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectRegionList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultRegion.Message;
                    return View(foodInfoModels);
                }
                #endregion

                foodInfoModels.m_Food = new mFood();
                foodInfoModels.m_Food.OrderDate = DateTime.Now;

                return View(foodInfoModels);
            }
            catch (Exception ex)
            {
                ViewBag.UrlGoTo = Url.Action("Index", "Home");
                ViewBag.MsgError = ex.Message.Replace("\r\n", "");
                return View(foodInfoModels);
            }
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Add")]
        public ActionResult Add(FoodInfoModels foodInfoModels)
        {
            try
            {
                iHttpClient iHttpClient = new iHttpClient();

                #region Get Category
                var resultCategory = iHttpClient.GetAsync(UrlHelpers.categoryList);
                if (resultCategory.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_category>>>(resultCategory.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Category.ToString(), Value = e.Category.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectCategoryList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultCategory.Message;
                    return View(foodInfoModels);
                }
                #endregion

                #region Get City
                var resultCity = iHttpClient.GetAsync(UrlHelpers.cityList);
                if (resultCity.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_city>>>(resultCity.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.City.ToString(), Value = e.City.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectCityList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultCity.Message;
                    return View(foodInfoModels);
                }
                #endregion

                #region Get region
                var resultRegion = iHttpClient.GetAsync(UrlHelpers.regionList);
                if (resultRegion.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<List<z_region>>>(resultRegion.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        return View(foodInfoModels);
                    }
                    else
                    {
                        List<SelectListItem> li = new List<SelectListItem>();
                        foreach (var e in resultModel.data)
                        {
                            li.Add(new SelectListItem { Text = e.Region.ToString(), Value = e.Region.ToString() });
                        }
                        var emptip = new SelectListItem()
                        {
                            Value = "",
                            Text = "[กรุณาเลือก]"
                        };
                        li.Insert(0, emptip);
                        foodInfoModels.SelectRegionList = li;
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = resultRegion.Message;
                    return View(foodInfoModels);
                }
                #endregion

                foodInfoModels.m_Food.TotalPrice = foodInfoModels.m_Food.Quantity * foodInfoModels.m_Food.UnitPrice;

                var result = iHttpClient.GetPostAsync(UrlHelpers.foodAdd, foodInfoModels.m_Food);
                if (result.StatusCode == 200)
                {
                    var resultModel = JsonConvert.DeserializeObject<OutputModels<bool>>(result.Message);
                    if (resultModel.error_code != 1 || resultModel.sub_code != 1)
                    {
                        ViewBag.MsgError = string.Format("{0} {1} {2}", resultModel.error_code, resultModel.sub_code, resultModel.message);
                        ViewBag.UrlGoTo = Url.Action("Index", "Home");
                        return View(foodInfoModels);
                    }
                    else
                    {
                        ViewBag.MsgSuccess = resultModel.message;
                        ViewBag.UrlGoTo = Url.Action("Index", "Home");
                        return View(foodInfoModels);
                    }
                }
                else
                {
                    ViewBag.UrlGoTo = Url.Action("Index", "Home");
                    ViewBag.MsgError = result.Message;
                    return View(foodInfoModels);
                }

                return View(foodInfoModels);
            }
            catch (Exception ex)
            {
                ViewBag.UrlGoTo = Url.Action("Index", "Home");
                ViewBag.MsgError = ex.Message.Replace("\r\n", "");
                return View(foodInfoModels);
            }
        }

    }
}