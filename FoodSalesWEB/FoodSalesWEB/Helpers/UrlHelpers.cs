﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesWEB.Helpers
{
    public class UrlHelpers
    {
        public static string urlFirstPart => @"https://localhost:44356/";
        public static string foodList => @"food/list";



        public static string categoryList => @"category/list";
        public static string cityList => @"city/list";
        public static string regionList => @"region/list";
        public static string foodByID => @"food/by/";
        public static string foodEdit => @"food/edit";
        public static string foodDelete => @"food/delete/";
        public static string foodAdd => @"food/add";
        public static string foodSearch => @" food/Search"; 
    }
}

