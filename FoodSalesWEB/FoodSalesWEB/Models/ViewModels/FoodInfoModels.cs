﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FoodSalesWEB.Models.ViewModels
{
    public class FoodInfoModels
    {
        public mFood m_Food { get; set; }

        /// <summary>
        ///  Drop Down category
        /// </summary>
        public IEnumerable<SelectListItem> SelectCategoryList { get; set; }

        /// <summary>
        ///  Drop Down City
        /// </summary>
        public IEnumerable<SelectListItem> SelectCityList { get; set; }

        /// <summary>
        ///  Drop Down Region
        /// </summary>
        public IEnumerable<SelectListItem> SelectRegionList { get; set; }
    }


    public class mFood
    {
        /// <summary>
        /// รหัสสินค้า
        /// </summary>
        public int foodID { get; set; }
        /// <summary>
        /// วันที่
        /// </summary>
        public DateTime OrderDate { get; set; }
        /// <summary>
        /// ภูมิภาค
        /// </summary>
        [Required]
        public string Region { get; set; }
        /// <summary>
        /// เมือง
        /// </summary>
        [Required]
        public string City { get; set; }
        /// <summary>
        /// Category
        /// </summary>
        [Required]
        public string Category { get; set; }
        /// <summary>
        /// ชื่อสินค้า
        /// </summary>
        [Required]
        public string Product { get; set; }
        /// <summary>
        /// จำนวน
        /// </summary>
        [Required]
        public int Quantity { get; set; } = 1;
        /// <summary>
        /// ราคาต่อหน่วย
        /// </summary>
        [Required]
        public double UnitPrice { get; set; } = 1.0;
        /// <summary>
        /// ราคารวม
        /// </summary>
        public double TotalPrice { get; set; }
    }

}