﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace FoodSalesWEB.Models
{
    public class FoodModels
    {
        public List<m_food> m_Foods { get; set; }

        // <summary>
        ///  Drop Down category
        /// </summary>
        public IEnumerable<SelectListItem> SelectCategoryList { get; set; }

        /// <summary>
        /// DropDown category
        /// </summary>
        public string SelectCategory { get; set; }

        // <summary>
        ///  Drop Down City
        /// </summary>
        public IEnumerable<SelectListItem> SelectCityList { get; set; }

        /// <summary>
        /// DropDown City
        /// </summary>
        public string SelectCity { get; set; }

        // <summary>
        ///  Drop Down Region
        /// </summary>
        public IEnumerable<SelectListItem> SelectRegionList { get; set; }

        /// <summary>
        /// DropDown Region
        /// </summary>
        public string SelectRegion { get; set; }

        [Display(Name = "Search By Product Name :")]
        public string ProductNameSearch { get; set; }

        [Display(Name = "วันสั่งเริ่ม :")]
        public string OrderDateStart { get; set; }
        [Display(Name = "วันสั่งสิ้นสุด :")]
        public string OrderDateEnd { get; set; }

    }
}