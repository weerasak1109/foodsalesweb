﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesWEB.Models.API
{
    /// <summary>
    /// 
    /// </summary>
    public class OutputModels<TValue>
    {
        /// <summary>
		/// รหัสข้อผิดพลาด
		/// </summary>
		public int error_code { get; set; } 
        /// <summary>
        /// รหัสย่อยข้อผิดพลาด
        /// </summary>
        public int sub_code { get; set; } 
        /// <summary>
        /// หัวเรื่อง
        /// </summary>
        public string title { get; set; } 
        /// <summary>
        /// ข้อความ ข้อผิดพลาด
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// ข้อมูล
        /// </summary>
        public TValue data { get; set; }
    }
}