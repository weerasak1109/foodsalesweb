﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesWEB.Models.API
{
    public class iHttpClientModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class FoodSearchModelsIn
    {
        public string SearchCategory { get; set; }
        public string SearchCity { get; set; }
        public string SearchRegion { get; set; }
        public string ProductNameSearch { get; set; }
        public string OrderDateStart { get; set; }
        public string OrderDateEnd { get; set; }
    }

}