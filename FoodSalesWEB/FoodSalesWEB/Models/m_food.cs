﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesWEB.Models
{
    public class m_food 
    {
        /// <summary>
        /// รหัสสินค้า
        /// </summary>
        public int foodID { get; set; }
        /// <summary>
        /// วันที่
        /// </summary>
        public DateTime OrderDate { get; set; }
        /// <summary>
        /// ภูมิภาค
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// เมือง
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Category
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// ชื่อสินค้า
        /// </summary>
        public string Product { get; set; }
        /// <summary>
        /// จำนวน
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// ราคาต่อหน่วย
        /// </summary>
        public double UnitPrice { get; set; }
        /// <summary>
        /// ราคารวม
        /// </summary>
        public double TotalPrice { get; set; }
    }

}